import { Component, OnInit } from '@angular/core';
import { GradServiceService } from '../../services/grad-service.service';
import { Grad } from '../../datamodel/Grad';
import { Router, ActivatedRoute } from '@angular/router';
import { skill } from '../../datamodel/skill';
@Component({
  selector: 'app-update-grad',
  templateUrl: './update-grad.component.html',
  styleUrls: ['./update-grad.component.css']
})
export class UpdateGradComponent implements OnInit {
  id: number;
  FinalList: any = [];
  skillList: any;
  grad: Grad;
  flag: any;
  gradSkill: skill = new skill();
  constructor(private route: ActivatedRoute, private router: Router, private gradService: GradServiceService) { }

  ngOnInit(): void {
    this.grad = new Grad();
    this.id = this.route.snapshot.params['id'];
    this.gradService.getGrad(this.id)
      .subscribe(data => {
        this.flag = "";
        for (let i = 0; i < data.skills.length; i++) {
          this.flag = this.flag + data.skills[i].skillName + ",";
        }
        this.grad = data;
        this.skillList = this.flag.substring(0, this.flag.length - 1);
      }, error => console.log(error));
  }
  Back() {
    this.router.navigate(['/gradDetail']);
  }
  submit() {
     const tokenOfUser = localStorage.getItem('name');
    this.grad.createdBy = tokenOfUser;
    let splitted = this.skillList.split(",");
    for (let i = 0; i < splitted.length; i++) {
      this.gradSkill = new skill();
      this.gradSkill.skillId = 1;
      this.gradSkill.skillName = splitted[i];
      this.gradSkill.createdBy = tokenOfUser;
      this.gradSkill.createdOn = new Date();
      this.FinalList[i] = this.gradSkill;
    }
    this.grad.skills = this.FinalList;
    this.grad.updatedBy = tokenOfUser;
    this.grad.createdOn = new Date();
    this.grad.updatedOn = new Date();
    console.log(this.grad);
    this.gradService.updateGrad(this.grad)
      .subscribe(data => console.log(data), error => console.log(error));
    this.gotoList();

  }
  gotoList() {
    this.router.navigate([`/gradDetail`]);
  }
}

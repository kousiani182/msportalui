import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { SkillTrendComponent } from './skill-trend.component';
import { RouterTestingModule } from '@angular/router/testing';
import { GradServiceService } from '../../services/grad-service.service';
import { of } from 'rxjs';
describe('SkillTrendComponent', () => {
  let component: SkillTrendComponent;
  let fixture: ComponentFixture<SkillTrendComponent>;
  let gradService :GradServiceService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule,RouterTestingModule],
      declarations: [ SkillTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Trend',async(()=>{
    const mockTrend=[{
      skill:'java',
      countOfGrad:2 },
    {
    skill: 'sql',
    countOfGrad:3 }
  ];
    gradService=TestBed.inject(GradServiceService);
    spyOn(gradService,"getTrendSkill").and.returnValue(of(JSON.stringify(mockTrend)));
    component.ngOnInit();
    expect(component.result).toEqual(mockTrend);
  }))



});

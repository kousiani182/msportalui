import { Component, OnInit } from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { GradServiceService } from '../../services/grad-service.service';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-skill-trend',
  templateUrl: './skill-trend.component.html',
  styleUrls: ['./skill-trend.component.css']
})
export class SkillTrendComponent implements OnInit {

  location: any = [];
  result:any=[];
  joiningLocation: any = [];
  skills: any = [];
  countOfSkill: any = [];
  count: any = [];
  countOfGrads: any = [];
  year: any = [];
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[];
  public pieChartData: SingleDataSet = [1, 2, 3];
  public pieChartLabels1: Label[];
  public pieChartData1: SingleDataSet = [1, 2, 3, 4,5 ,6];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];  

  constructor( private gradService: GradServiceService) {
       monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
   }

  ngOnInit(): void {

    this.gradService.getTrendSkill()
      .subscribe(data => {
        //this.result=JSON.parse(data);
        for (let i = 0; i < data.length; i++) {
          this.skills[i] = data[i].skill;
          
          this.countOfSkill[i] = data[i].countOfGrad;
        }
        this.pieChartLabels1 = this.skills;
        this.pieChartData1 = this.countOfSkill;
      }, error => console.log(error));


  }
  

}

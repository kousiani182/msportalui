import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginUser } from './datamodel/LoginUser';
@Injectable({

  providedIn: 'root'

})

export class SocialloginService {
  url;
  constructor(private http: HttpClient) { }
  Savesresponse(res) {
    let user = new LoginUser(res.email, res.name, res.idToken)
    console.log("user", user);
    this.url = '/api/login';
    return this.http.post(this.url, user);
  }

  isUserLoggedIn() {
    let user = localStorage.getItem('idToken')
    return !(user === null)
  }
}


import { Component, OnInit } from '@angular/core';
import { GradServiceService } from '../../services/grad-service.service';
import { Grad } from '../../datamodel/Grad';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-grad-list',
  templateUrl: './grad-list.component.html',
  styleUrls: ['./grad-list.component.css']
})
export class GradListComponent implements OnInit {
  id: number;
  grad: Grad;
  j: any;

  constructor(private route: ActivatedRoute, private router: Router, private gradService: GradServiceService) { }

  ngOnInit(): void {
    this.grad = new Grad();
    this.id = this.route.snapshot.params['id'];
    this.gradService.getGrad(this.id)
      .subscribe(data => {
        this.j = "";
        for (let i = 0; i < data.skills.length; i++) {
          this.j = this.j + data.skills[i].skillName + ",";
        }
        this.grad = data;
        this.grad.skills = this.j.substring(0, this.j.length - 1);
      }, error => console.log(error));
    console.log(this.grad);
  }
  Back() {
    this.router.navigate([`/gradDetail`]);
  }
}

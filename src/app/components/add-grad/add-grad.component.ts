import { Component, OnInit } from '@angular/core';
import { Grad } from '../../datamodel/Grad';
import { skill } from '../../datamodel/skill';
import { GradServiceService } from '../../services/grad-service.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-grad',
  templateUrl: './add-grad.component.html',
  styleUrls: ['./add-grad.component.css']
})
export class AddGradComponent implements OnInit {
  text='contact';
  skillList: String;
  FinalList:any=[];
  grad: Grad = new Grad();
  grad1: Grad =new Grad();
  skill:skill =new skill();
  submitted = false;
  gradFormGroup: FormGroup;
  constructor(private router:Router,private gradService: GradServiceService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
     this.gradFormGroup = this.formBuilder.group({
            gradFirstName: ['', Validators.required],
            gradMiddleName: ['', Validators.required],
            gradLastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            zipcode: ['', [Validators.required, Validators.minLength(6)]],
            gender: ['', Validators.required],
            state: ['', Validators.required],
            street: ['', Validators.required],
            contact:['',[Validators.required, Validators.maxLength(10)]],
            houseNo:['', Validators.required],
            city:['', Validators.required],
            branch:['', Validators.required],
            degree:['', Validators.required],
            institute:['', Validators.required],
            joiningLocation:['', Validators.required],
            joiningDate:['', Validators.required],
            feedback:['', Validators.required],
            skills:['', Validators.required]
            
        });
  }
   get f() { return this.gradFormGroup.controls; }
   newGrad(): void {
    this.submitted = false;
    this.grad= new Grad();
  }

   save() {
    this.grad1= new Grad();
    this.gradService.createGrad(this.grad)
      .subscribe(data => console.log(data), error => console.log(error));
    this.grad = new Grad();
    this.gotoList();
  }

  onSubmit() {
    const tokenOfUser = localStorage.getItem('name');
    this.grad.createdBy=tokenOfUser; 
    if(this.skillList && this.skillList.includes(",")){
    let splittedSkill=this.skillList.split(",");
    for(let i=0;i<splittedSkill.length;i++){
     this.skill=new skill();
     this.skill.skillId=1;
     this.skill.skillName=splittedSkill[i];
     this.skill.createdBy=tokenOfUser;
     this.skill.createdOn=new Date();
     this.FinalList[i]=this.skill;
    }
    }
    this.grad.skills=this.FinalList;
    this.grad.updatedBy=tokenOfUser;
    this.grad.createdOn=new Date();
    this.grad.updatedOn=new Date();
    this.submitted = true;
    this.save();    
  }
  Back()
  {
    this.router.navigate([`/gradDetail`]); 
  }
  gotoList() {
   this.router.navigate([`/gradDetail`]); 
  }
}

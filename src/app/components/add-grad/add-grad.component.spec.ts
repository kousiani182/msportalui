import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { AddGradComponent } from './add-grad.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';  
import { ReactiveFormsModule} from '@angular/forms';
import { GradServiceService } from '../../services/grad-service.service'; 
import { observable, of } from 'rxjs';
import { Router } from '@angular/router';

describe('AddGradComponent', () => {
  let component: AddGradComponent;
  let fixture: ComponentFixture<AddGradComponent>;
  let service: GradServiceService;
  let router:Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[RouterTestingModule,HttpClientModule,FormsModule,ReactiveFormsModule],
      declarations: [ AddGradComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGradComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set submitted to false',async(()=>
  {
   
    console.log(component.submitted);
    expect(component.submitted).toEqual(false);
  }));

    it('form should be invalid',async(()=>{
    component.gradFormGroup.controls['email'].setValue('');
    expect(component.gradFormGroup.valid).toBeFalsy();
   }));

 it('should check working of get  methods of gradForm',async( () =>{
   
   expect(component.gradFormGroup.controls['gradFirstName']).toBeTruthy();
   expect(component.gradFormGroup.controls['gradMiddleName']).toBeTruthy();
   expect(component.gradFormGroup.controls['gradLastName']).toBeTruthy();
   expect(component.gradFormGroup.controls['email']).toBeTruthy();
   expect(component.gradFormGroup.controls['contact']).toBeTruthy();

  }));

   it('form should be valid',async(()=>
   {
    component.gradFormGroup.controls['gradFirstName'].setValue('Gowsalya');
    component.gradFormGroup.controls['gradMiddleName'].setValue('S');
    component.gradFormGroup.controls['gradLastName'].setValue('G');
    component.gradFormGroup.controls['email'].setValue('gowsalya@gmail.com');
    component.gradFormGroup.controls['contact'].setValue('8989898989');
    component.gradFormGroup.controls['zipcode'].setValue('623701');
    component.gradFormGroup.controls['gender'].setValue('female');
    component.gradFormGroup.controls['state'].setValue('TamilNadu');
    component.gradFormGroup.controls['street'].setValue('nagar');
    component.gradFormGroup.controls['houseNo'].setValue('12');
    component.gradFormGroup.controls['city'].setValue('pmk');
    component.gradFormGroup.controls['branch'].setValue('IT');
    component.gradFormGroup.controls['degree'].setValue('BTECH');
    component.gradFormGroup.controls['institute'].setValue('GCT');
    component.gradFormGroup.controls['joiningLocation'].setValue('bangalore');
    component.gradFormGroup.controls['joiningDate'].setValue('2020-01-08');
    component.gradFormGroup.controls['feedback'].setValue('good');
    component.gradFormGroup.controls['skills'].setValue('java,angular');
    expect(component.gradFormGroup.valid).toBeTruthy();
   }));

   it('should check working of onSubmit method',async(()=>{
      component.gradFormGroup.controls['gradFirstName'].setValue('Gowsalya');
    component.gradFormGroup.controls['gradMiddleName'].setValue('S');
    component.gradFormGroup.controls['gradLastName'].setValue('G');
    component.gradFormGroup.controls['email'].setValue('gowsalya@gmail.com');
    component.gradFormGroup.controls['contact'].setValue('8989898989');
    component.gradFormGroup.controls['zipcode'].setValue('623701');
    component.gradFormGroup.controls['gender'].setValue('female');
    component.gradFormGroup.controls['state'].setValue('TamilNadu');
    component.gradFormGroup.controls['street'].setValue('nagar');
    component.gradFormGroup.controls['houseNo'].setValue('12');
    component.gradFormGroup.controls['city'].setValue('pmk');
    component.gradFormGroup.controls['branch'].setValue('IT');
    component.gradFormGroup.controls['degree'].setValue('BTECH');
    component.gradFormGroup.controls['institute'].setValue('GCT');
    component.gradFormGroup.controls['joiningLocation'].setValue('bangalore');
    component.gradFormGroup.controls['joiningDate'].setValue('2020-01-08');
    component.gradFormGroup.controls['feedback'].setValue('good');
    component.gradFormGroup.controls['skills'].setValue('java,angular');
    expect(component.gradFormGroup.valid).toBeTruthy();
    service=TestBed.inject(GradServiceService);
    router=TestBed.inject(Router);
    spyOn(localStorage, 'getItem').and.returnValue("someValue");
    spyOn(service,"createGrad").and.returnValue(of("something"));
    spyOn(router,"navigateByUrl");
    component.onSubmit();
    spyOn(service,"getGrad").and.returnValue(of("something"));

  }))


});

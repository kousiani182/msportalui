import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http'
import { GradServiceService } from './grad-service.service';
import { RouterTestingModule } from '@angular/router/testing';
describe('GradServiceService', () => {
  let service: GradServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule,RouterTestingModule]
    });
    service = TestBed.inject(GradServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { GradServiceService } from '../../services/grad-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Grad } from '../../datamodel/Grad';
import { Router } from '@angular/router';
@Component({
  selector: 'app-grad-detail',
  templateUrl: './grad-detail.component.html',
  styleUrls: ['./grad-detail.component.css']
})
export class GradDetailComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  gradDetails;
  RowDefs; db;
  ColunmDef: string[] = ['gradId', 'name', 'branch', 'institute', 'joiningDate', 'joiningLocation', 'deleteButton', 'infoButton', 'editButton', 'HistoryButton'];
  constructor(private router: Router, private gradService: GradServiceService) { }

  ngOnInit(): void {
      this.gradService.getGrads().subscribe((data) => {
      this.gradDetails = data;
      this.db = new MatTableDataSource<Grad>(this.gradDetails);
      //this.db.data=this.gradDetails;
      this.db.paginator = this.paginator;
      this.db.sort = this.sort;
    })
  }
  deleteGrad(id: number) {
    this.gradService.deleteGradById(id);
  }

  Grad(id: number) {
    this.router.navigate(['detailsGrad', id]);
  }
  searching(event: Event) {
    this.db.filter = (event.target as HTMLInputElement).value.trim();
  }

}


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SocialLoginModule } from 'angularx-social-login';
import { HttpClientModule } from '@angular/common/http';
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { GradListComponent } from './components/grad-list/grad-list.component';
import { AddGradComponent } from './components/add-grad/add-grad.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateGradComponent } from './components/update-grad/update-grad.component';
import { GradDetailComponent } from './components/grad-detail/grad-detail.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon'
import { MatSelectModule } from '@angular/material/select';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { HistoryComponent } from './components/history/history.component';
import { ChartsModule } from 'ng2-charts';
import { TrendsComponent } from './components/trends/trends.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { MatCardModule } from '@angular/material/card';
import { LocationTrendComponent } from './components/location-trend/location-trend.component';
import { SkillTrendComponent } from './components/skill-trend/skill-trend.component';
import { TrendToolBarComponent } from './components/trend-tool-bar/trend-tool-bar.component';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('884334067180-bqtvob7cu3r40s7pomh03c4dg56e55vg.apps.googleusercontent.com')
  },

]);

export function provideConfig() {
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    GradListComponent,
    AddGradComponent,
    UpdateGradComponent,
    GradDetailComponent,
    HistoryComponent,
    TrendsComponent,
    ToolbarComponent,
    LocationTrendComponent,
    SkillTrendComponent,
    TrendToolBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    SocialLoginModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    ChartsModule,
    MatCardModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {

      provide: AuthServiceConfig,
      useFactory: provideConfig
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }

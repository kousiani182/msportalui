import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { GradDetailComponent } from './grad-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { gradServiceMock } from '../../mocks/grad.service'

describe('GradDetailComponent', () => {
  let component: GradDetailComponent;
  let fixture: ComponentFixture<GradDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[RouterTestingModule,HttpClientModule],
      declarations: [ GradDetailComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  }); 
  
  

});

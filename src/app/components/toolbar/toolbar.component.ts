import { Component, OnInit } from '@angular/core';
import { SocialloginService } from '../../sociallogin.service';  
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
 isUserLoggedIn: boolean = false;
  constructor(private loginService : SocialloginService) { }

  ngOnInit(): void {
    this.isUserLoggedIn = this.loginService.isUserLoggedIn();
  }

}


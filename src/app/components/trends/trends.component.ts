import { Component, OnInit } from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { GradServiceService } from '../../services/grad-service.service';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit {
 location: any = [];
  joiningLocation: any = [];
  skills: any = [];
  countOfSkill: any = [];
  count: any = [];
  countOfGrads: any = [];
  year: any = [];

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[];
  public pieChartData: SingleDataSet = [1, 2, 3];
  public pieChartLabels1: Label[];
  public pieChartData1: SingleDataSet = [1, 2, 3, 4,5 ,6];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  
  constructor(private gradService: GradServiceService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    this.gradService.getTrendLocation()
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.location[i] = data[i].joining_location;
          this.count[i] = data[i].countOfGrad;
        }
        this.pieChartLabels = this.location;
        this.pieChartData = this.count;
      }, error => console.log(error));

  }

}

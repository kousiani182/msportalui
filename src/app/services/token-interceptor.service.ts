import { Injectable } from '@angular/core';
import { HttpHandler, HttpRequest, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService {

  constructor() { }
  intercept(request: HttpRequest<any>, handler: HttpHandler): Observable<HttpEvent<any>> {
    const tokenOfUser = localStorage.getItem('idToken');
    const req = request.clone({
      headers: request.headers.set('idToken', tokenOfUser),
    });
    return handler.handle(req);
  }
}

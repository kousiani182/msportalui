import { Component, OnInit } from '@angular/core';
import { GradServiceService } from '../../services/grad-service.service';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
@Component({
  selector: 'app-location-trend',
  templateUrl: './location-trend.component.html',
  styleUrls: ['./location-trend.component.css']
})
export class LocationTrendComponent implements OnInit {
 
  location: any = [];
  joiningLocation: any = [];
  skills: any = [];
  countOfSkill: any = [];
  count: any = [];
  countOfGrads: any = [];
  year: any = [];
  result :any=[];

  public barChartOptions: ChartOptions = {
    scales: {
        yAxes: [{id: 'y-axis-1', type: 'linear', position: 'left', ticks: {min: 0, max:50}}]
      },
    responsive: true,
  };
  public barChartLabels: Label[] = ['2015', '2016', '2017', '2018', '2019', '2020'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [], label: 'bangalore' },
    { data: [], label: 'chennai' },
    { data: [], label: 'pmk' },
    { data: [], label: 'kolkata' },
  ];

  constructor(private gradService: GradServiceService) { }

  ngOnInit(): void {

    
    this.gradService.getTrend().
      subscribe(data => {
        //this.result=JSON.parse(data);
        for (let i = 0; i < data.length; i++) {
          for (let j = 0; j < this.barChartLabels.length; j++) {
            if (this.barChartLabels[j] === data[i].joining_date) {
              if (data[i].joining_location === "bangalore" || data[i].joining_location === "Bangalore") {
                this.barChartData[0].data[j] = data[i].countOfGrad;
              }
              if (data[i].joining_location === "chennai") {
                this.barChartData[1].data[j] = data[i].countOfGrad;
              }
              if (data[i].joining_location === "pmk") {
                this.barChartData[2].data[j] = data[i].countOfGrad;
              }
              if (data[i].joining_location === "kolkata") {
                this.barChartData[3].data[j] = data[i].countOfGrad;
              }
            }
          }
        }
      }, error => console.log(error))
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { GradDetailComponent } from './components/grad-detail/grad-detail.component';
import { AddGradComponent } from './components/add-grad/add-grad.component';
import { GradListComponent } from './components/grad-list/grad-list.component';
import { UpdateGradComponent } from './components/update-grad/update-grad.component';
import { HistoryComponent } from './components/history/history.component';
import { TrendsComponent } from './components/trends/trends.component';
import { SkillTrendComponent } from './components/skill-trend/skill-trend.component';
import { TrendToolBarComponent } from './components/trend-tool-bar/trend-tool-bar.component';
import { LocationTrendComponent } from './components/location-trend/location-trend.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'gradDetail', component: GradDetailComponent },
  { path: 'addGrad', component: AddGradComponent },
  { path: 'detailsGrad/:id', component: GradListComponent },
  { path: 'updateGrad/:id', component: UpdateGradComponent },
  { path: 'historyGrad/:id', component: HistoryComponent },
  { path: 'trends', component: TrendsComponent },
  { path: 'skillTrend', component: SkillTrendComponent },
  { path: 'locationYrTrend', component: LocationTrendComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

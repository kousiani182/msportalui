import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Grad } from '../datamodel/Grad';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class GradServiceService {
  private url = '/api/gradHistory';
  private baseUrl = '/api/grad';
  private baseUrl1 = '/api/deleteGrad';
  constructor(private http: HttpClient, private router: Router) { }

  updateGrad(employee: Grad) {
    return this.http.post('/api/updateGrad', employee);
  }
  getTrendSkill(): Observable<any> {
    return this.http.get('/api/GetTrendBySkill');
  }
  getTrendLocation(): Observable<any> {
    return this.http.get('/api/getTrendByLocation');
  }
  getTrend(): Observable<any> {
    return this.http.get('/api/getTrend');
  }
  HistoryGrad(id: number): Observable<any> {
    if(id){
    return this.http.get(`${this.url}/${id}`);
    }
  }
  getGrads() {
    return this.http.get('/api/grad');
  }

  createGrad(employee: Grad) {
    return this.http.post('/api/addGrad', employee);
  }

  getGradById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  getGrad(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  deleteGradById(id: number) {
    let endPoints = `${this.baseUrl1}/${id}`;
    this.http.delete(endPoints).subscribe(data => {
      console.log(data);
    });
    this.router.navigate(['/gradDetail']);
  }

}

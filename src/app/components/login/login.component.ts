import { Component, OnInit } from '@angular/core';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angularx-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { Socialusers } from '../../socialusers'
import { SocialloginService } from '../../sociallogin.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  response;
  socialusers = new Socialusers();
  constructor(
    public OAuth: AuthService,
    private SocialloginService: SocialloginService,
    private router: Router,
  ) { }
  ngOnInit() {
  }
  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
      console.log(socialProvider, socialusers);
      console.log("login response", socialusers);

      this.Savesresponse(socialusers);
    });
  }
  Savesresponse(socialusers: Socialusers) {
    
    localStorage.setItem('idToken', socialusers.idToken);
    this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {
      localStorage.setItem('name',res.name);
      localStorage.setItem('user', res.email);
      this.router.navigate([`/gradDetail`]);
    })
  }
  logout() {
    alert(1);
    this.OAuth.signOut();
  }
}

export class Grad {
    public gradId: number;
    public gradFirstName: String;
    public gradMiddleName: String;
    public gradLastName: String;
    public gender: String;
    public email: String;
    public contact: number;
    public houseNo: String;
    public street: String;
    public city: String;
    public state: String;
    public zipcode: String;
    public institute: String;
    public degree: String;
    public branch: String;
    public feedback: String;
    public skills: [];
    public joiningDate: Date;
    public joiningLocation: String;
    public createdBy: String;
    public createdOn: Date;
    public updatedBy: String;
    public updatedOn: Date;

}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { LocationTrendComponent } from './location-trend.component';
import { RouterTestingModule } from '@angular/router/testing';
import { GradServiceService } from '../../services/grad-service.service';
import { of } from 'rxjs';
describe('LocationTrendComponent', () => {
  let component: LocationTrendComponent;
  let fixture: ComponentFixture<LocationTrendComponent>;
  let gradService:GradServiceService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule,RouterTestingModule],
      declarations: [ LocationTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => { 
    expect(component).toBeTruthy();
  });

   it('should get Trend',async(()=>{
    const mockTrend=[{
     joining_location:"bangalore",
     joining_date:"2000",
     countOfGrad:2 },
  ];
    gradService=TestBed.inject(GradServiceService);
    spyOn(gradService,"getTrend").and.returnValue(of(JSON.stringify(mockTrend)));
    component.ngOnInit();
    expect(component.result).toEqual(mockTrend);
  }))
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { GradServiceService } from '../../services/grad-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Grad } from '../../datamodel/Grad';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  gradDetails;
  RowDefs;
  db;
  id: number;
  ColunmDef: string[] = ['gradId', 'name', 'branch', 'institute', 'joiningDate', 'joiningLocation'];
  constructor(private route: ActivatedRoute, private router: Router, private gradService: GradServiceService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.gradService.HistoryGrad(this.id).subscribe((data) => {
      this.gradDetails = data;
      this.db = new MatTableDataSource<Grad>(this.gradDetails);
      this.db.paginator = this.paginator;
      this.db.sort = this.sort;
    })
  }
  Back() {
    this.router.navigate([`/gradDetail`]);
  }
}



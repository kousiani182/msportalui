import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendToolBarComponent } from './trend-tool-bar.component';

describe('TrendToolBarComponent', () => {
  let component: TrendToolBarComponent;
  let fixture: ComponentFixture<TrendToolBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendToolBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendToolBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

 
});

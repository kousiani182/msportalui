import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { UpdateGradComponent } from './update-grad.component';
import { RouterTestingModule } from '@angular/router/testing';
describe('UpdateGradComponent', () => {
  let component: UpdateGradComponent;
  let fixture: ComponentFixture<UpdateGradComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[RouterTestingModule,HttpClientModule],
      declarations: [ UpdateGradComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGradComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

   it('should update Grad',async(()=>
   {
     const compiled=fixture.debugElement.nativeElement;
     expect(compiled.querySelector('h1').textContent).toContain('UPDATE GRAD');
   }));

});
